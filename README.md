Tento program slouzi k ukladani dat o turistickych smerovkach, ktera jsou pouzivana k vypisovani dokumentu pro KCT.
Ma za ukol zjednodusit praci znackarum, kteri jinak musi uchovavat tyto udaje na papire, coz je znacne nepohodlne, jelikoz se obnovuji jednou za 3 roky.

Mezi funkce programu patri vytvoreni, smazani, vyhledani, nebo uprava smerovky.
Data se ukladaji do souboru "databaze.txt", ktery se nachazi ve slozce s programem.

Pro seznam prikazu dostupnych v programu a dalsi informace zadejte primo v programu prikaz 'help'.
