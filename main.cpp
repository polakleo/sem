#include <iostream>
#include <vector>
#include <string>
#include <exception>
#include <istream>
#include <ostream>
#include <fstream>

using namespace std;

struct Smerovka
{
    string smer;
    string barva;
    string nazev;
    string kod;
    int rok;
    int stav;
    Smerovka(string s, string b, string n, string k, int r, int st) : smer(s), barva(b), nazev(n), kod(k), rok(r), stav(st){

                                                                                                                   };
};

void vypis(Smerovka sm) //vypsani tabulky s udaji o smerovce
{

    cout << "|=========================|\n";
    //1. radek
    cout << "|";
    int dn = sm.nazev.size();
    int mez = (25 - dn) / 2;
    for (int i = 0; i < mez; i++)
        cout << " ";
    cout << sm.nazev;
    for (int i = 0; i < mez; i++)
        cout << " ";
    if (dn % 2 == 0)
        cout << " ";
    cout << "|\n";
    //2. radek
    if (sm.smer == "n")
        cout << "|          nazev          |\n";

    else if (sm.smer == "l")
    {
        cout << "|<< " << sm.barva;
        ;
        int dbv = sm.barva.size();
        for (int i = 0; i < (22 - dbv); i++)
            cout << " ";
        cout << "|\n";
    }
    else
    {
        cout << "|";
        int dbv = sm.barva.size();
        for (int i = 0; i < (22 - dbv); i++)
            cout << " ";
        cout << sm.barva << " >>|\n";
    }
    //3.radek
    cout << "|" << sm.rok;
    ;
    int dk = sm.kod.size();
    for (int i = 0; i < (21 - dk); i++)
        cout << " ";
    cout << sm.kod << "|\n";
    //4. radek
    cout << "|stav: " << sm.stav << "                  |\n";

    cout << "|=========================|\n";
}

class Databaze
{
    vector<Smerovka> data;

public:
    void novaSmerovka(string s, string b, string n, string k, int r, int st) //vytvoreni smerovky
    {
        Smerovka sm = Smerovka(s, b, n, k, r, st);
        data.push_back(sm);
    }
    int najdi(string kod) //hledani smerovky podle kodu
    {
        int vel = data.size();
        int nal = 0;
        for (int i = 0; i < vel; i++)
        {
            Smerovka sm = data.at(i);
            if (sm.kod == kod)
            {
                nal = 1;
                vypis(sm);
            }
        }
        if (nal == 0)
        {
            return 0;
        }
        return 1;
    }
    void vymaz(string kod) //smazani smerovky
    {
        int vel = data.size();
        int nal = 0;
        int id;
        for (int i = 0; i < vel; i++)
        {
            Smerovka sm = data.at(i);
            if (sm.kod == kod)
            {
                nal = 1;
                id = i;
            }
        }
        if (nal == 1)
        {
            data.erase(data.begin() + id);
            cout << "Ulozeno!\n";
        }
        else
            cout << "Nenalezeno!\n";
    }
    void uprav(string kod) //uprava dat smerovky
    {
        int vel = data.size();
        int nal = 0;
        for (int i = 0; i < vel; i++)
        {
            Smerovka sm = data.at(i);
            if (sm.kod == kod)
            {
                nal = 1;
                vypis(sm);

                string s = sm.smer;
                string b = sm.barva;
                string n = sm.nazev;
                string k = sm.kod;
                int r, st;
            ROK_:
                cout << k << ": Zadejte rok vyroby:\n>";
                string rok;
                cin >> rok;
                try
                {
                    r = stoi(rok);
                }
                catch (const std::exception &e)
                {
                }

                if (r < 1000 || r > 9999)
                {
                    cout << "Zadejte cislo v rozmezi 1000 az 9999!\n";
                    goto ROK_;
                }
            STAV_:
                cout << k << ": Zadejte stav [1-4]:\n>";
                string stav;
                cin >> stav;
                try
                {
                    st = stoi(stav);
                }
                catch (const std::exception &e)
                {
                }
                if (st < 1 || st > 4)
                {
                    cout << "Zadejte cislo v rozmezi 1 az 4!\n";
                    goto STAV_;
                }

                vymaz(kod);
                novaSmerovka(s, b, n, k, r, st);
                break;
            }
        }
        if (nal == 0)
            cout << "Nenalezeno!\n";
    }
    void uloz() //ulozeni databaze do souboru "databaze.txt"
    {
        ofstream nf;
        nf.open("databaze.txt");
        int vel = data.size();
        for (int i = 0; i < vel; i++)
        {
            nf << data[i].kod << " " << data[i].smer << " " << data[i].barva << " " << data[i].nazev << " " << data[i].rok << " " << data[i].stav << endl;
        }
        nf.close();
        cout << "Zmeny ulozeny!\n";
    }
    void nacti() //nacteni databaze ze souboru "databaze.txt"
    {
        ifstream file;
        file.open("databaze.txt");
        string smerovka;
        while (getline(file, smerovka))
        {
            size_t pos1;
            pos1 = smerovka.find(" ");
            string kod = smerovka.substr(0, pos1);
            string temp = smerovka.substr(pos1 + 1);
            size_t pos2 = temp.find(" ");
            string smer = temp.substr(0, pos2);
            string temp2 = temp.substr(pos2 + 1);
            size_t pos3 = temp2.find(" ");
            string barva = temp2.substr(0, pos3);
            temp = temp2.substr(pos3 + 1);
            size_t pos4 = temp.find(" ");
            string nazev = temp.substr(0, pos4);
            temp2 = temp.substr(pos4 + 1);
            size_t pos5 = temp2.find(" ");
            string rtemp = temp2.substr(0, pos5);
            string sttemp = temp2.substr(pos5 + 1);

            int rok, stav;
            try
            {
                rok = stoi(rtemp);
                stav = stoi(sttemp);
            }
            catch (const std::exception &e)
            {
                std::cerr << e.what() << '\n';
            }

            novaSmerovka(smer, barva, nazev, kod, rok, stav);
        }
        file.close();
    }
};

int main(int argc, char **argv)
{
    string arg;
    Databaze db;
    db.nacti();
    cout << "Pro napovedu zadejte \"help\".\n";
    while (getline(std::cin, arg))
    {
        //nacitani prikazu a argumentu
        size_t position;
        if ((position = arg.find(" ")) == std::string::npos)
        {
            //prikazy bez argumentu
            if (arg == "new") //nova smerovka
            {
                string s, b, n, k;
                int r, st;
            SMER:
                cout << "Zadejte smer [l, p, n]:\n>";
                cin >> s;
                if (s != "l" && s != "p" && s != "n")
                {
                    cout << "Neplatny smer!\n";
                    goto SMER;
                }
                if (s == "n")
                {
                    b = "n";
                    goto CIL;
                }
            BARVA:
                cout << "Zadejte barvu [c, m, z, zl]:\n>";
                cin >> b;
                if (b == "c")
                    b = "cervena";
                else if (b == "m")
                    b = "modra";
                else if (b == "z")
                    b = "zelena";
                else if (b == "zl")
                    b = "zluta";
                else
                {
                    cout << "Neplatna barva!\n";
                    goto BARVA;
                }
            CIL:
                if (s == "n")
                    cout << "Zadejte nazev:\n>";
                else
                    cout << "Zadejte cil:\n>";
                cin >> n;
            KOD:
                cout << "Zadejte kod:\n>";
                cin >> k;
                if(db.najdi(k) == 1)
                {
                    cout << "Smerovka s timto kodem jiz existuje!\n";
                    goto KOD;
                }
            ROK:
                cout << "Zadejte rok vyroby:\n>";
                string rok;
                cin >> rok;
                try
                {
                    r = stoi(rok);
                }
                catch (const std::exception &e)
                {
                }

                if (r < 1000 || r > 9999)
                {
                    cout << "Zadejte cislo v rozmezi 1000 az 9999!\n";
                    goto ROK;
                }

            STAV:
                cout << "Zadejte stav [1-4]:\n>";
                string stav;
                cin >> stav;
                try
                {
                    st = stoi(stav);
                }
                catch (const std::exception &e)
                {
                }
                if (st < 1 || st > 4)
                {
                    cout << "Zadejte cislo v rozmezi 1 az 4!\n";
                    goto STAV;
                }

                db.novaSmerovka(s, b, n, k, r, st);
                cout << "Ulozeno!\n";
            }
            else if (arg == "find") //hledani smerovky
            {
                string code;
                cout << "Zadejte kod:\n>";
                cin >> code;
                if (db.najdi(code) == 0)
                    cout << "Nenalezeno!\n";
            }
            else if (arg == "remove") //mazani smerovky
            {
                string code;
                cout << "Zadejte kod:\n>";
                cin >> code;
                db.vymaz(code);
            }
            else if (arg == "edit") //uprava smerovky
            {
                string code;
                cout << "Zadejte kod:\n>";
                cin >> code;
                db.uprav(code);
            }
            else if (arg == "save") //ukladani databaze
            {
                db.uloz();
            }
            else if (arg == "help") //napoveda
            {
                cout << "\nPRIKAZY:\n";
                cout << "new -- nova smerovka\n";
                cout << "find (+argument) -- najdi smerovku, argumentem je kod\n";
                cout << "remove (+argument) -- vymaz smerovku, argumentem je kod\n";
                cout << "edit (+argument) -- prepis rok vyroby a stav smerovky, argumentem je kod\n";
                cout << "save -- ulozi databazi do souboru \"databaze.txt\"\n";
                cout << "exit -- ukonci program, zepta se na ulozeni zmen\n";
                cout << "INFO:\n";
                cout << "Kazda smerovka ma smer, barvu, nazev mista/cile, rok vyroby, stav a svuj unikatni kod.\n";
                cout << "Zadejte prikaz:\n";
            }
            else if (arg == "exit") //konec programu
            {
            ULOZ:
                cout << "Ulozit zmeny? [a/n]\n>";
                string str;
                cin >> str;
                if (str == "a")
                {
                    db.uloz();
                    return 0;
                }
                else if (str == "n")
                {
                    return 0;
                }
                else
                {
                    cout << "Neplatny argument!\n";
                    goto ULOZ;
                }
            }
            else if (arg == "") //prazdny argument
                continue;
            else
            {
                cout << "Neznamy prikaz!\n";
            }
        }
        else
        {
            //prikazy s argumentem
            std::string command = arg.substr(0, position);
            std::string argument = arg.substr(position + 1);
            if (command == "find")
            {
                if(db.najdi(argument)==0)
                    cout << "Nenalezeno!\n";
            }
            else if (command == "remove")
            {
                db.vymaz(argument);
            }
            else if (command == "edit")
            {
                db.uprav(argument);
            }
            else if (command == "new" || command == "save" || command == "exit")
            {
                cout << "Neplatne zadani!\n";
            }
            else
            {
                cout << "Neznamy prikaz!\n";
            }
        }
    }
    return 0;
}
